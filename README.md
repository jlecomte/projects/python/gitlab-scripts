[![license](https://img.shields.io/badge/license-MIT-brightgreen)](https://spdx.org/licenses/MIT.html)
[![documentation](https://img.shields.io/badge/documentation-html-informational)](https://gitlabsolute.docs.cappysan.dev)
[![pipelines](https://gitlab.com/cappysan/gitlabsolute/badges/master/pipeline.svg)](https://gitlab.com/cappysan/gitlabsolute/pipelines)
[![coverage](https://gitlab.com/cappysan/gitlabsolute/badges/master/coverage.svg)](https://gitlabsolute.docs.cappysan.dev/coverage/index.html)

# gitlabsolute

Helper and convenience scripts to help with common GitLab actions.

## Installation

You can install the latest version from PyPI package repository.

~~~bash
python3 -mpip install -U gitlabsolute
~~~


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Documentation: [https://gitlabsolute.docs.cappysan.dev](https://gitlabsolute.docs.cappysan.dev)
  * Website: [https://gitlab.com/cappysan/gitlabsolute](https://gitlab.com/cappysan/gitlabsolute)
  * PyPi: [https://pypi.org/project/gitlabsolute](https://pypi.org/project/gitlabsolute)
