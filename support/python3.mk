PROJECT_DIR := $(shell tomlq .tool.setuptools.packages[0] < pyproject.toml)
PROJECT_EXTRA_DIRS :=

ifeq ($(PROJECT_DIR),null)
$(error PROJECT_DIR is empty)
endif

# ------------------------------------------------------------------------------
# Python variables
PYTHON ?= python3
PIP ?= pip3
PYTESTFLAGS ?= ## Python pytest flags
PYCOVFLAGS ?= ## Python coverage flags

TWINE_REPOSITORY ?= pypi ## PyPi repository to use
TWINEFLAGS ?= ## Python3 twine flags

# ==============================================================================
# Python3
# ==============================================================================
.PHONY: check test tests pytest
check test tests: pytest ##
pycheck pytest: public
	$(PYTHON) -m pytest -W ignore::DeprecationWarning \
	    --junitxml=public/reports/unit-tests.xml \
	    --html=public/pytest/index.html --self-contained-html \
	    $(PYTESTFLAGS) -- tests

.PHONY: coverage cov pycov
coverage cov: pycov ##
pycov: public
	$(PYTHON) -m pytest -W ignore::DeprecationWarning \
	    --cov=$(subst $() $(), --cov=,$(PROJECT_DIR)) \
	    --cov-report term-missing \
	    --cov-report=html:"$(shell pwd)/public/coverage/" \
	    --cov-report=xml:"$(shell pwd)/public/reports/coverage.xml" \
	    --junitxml=public/reports/unit-tests.xml \
	    --html=public/pytest/index.html --self-contained-html \
	    $(PYTESTFLAGS) $(PYCOVFLAGS) -- tests
	@echo "Coverage generated here: file://$(shell pwd)/public/coverage/index.html"

.PHONY: lint pylint
lint: pylint ##
pylint: public
	$(PYTHON) -m pylint $(PYLINTFLAGS) --recursive y \
	    $(PROJECT_DIR) $(PROJECT_EXTRA_DIRS) \
	    $(wildcard tests)

.PHONY: clean pyclean
clean: pyclean ##
pyclean:
	@find $(PROJECT_EXTRA_DIRS) $(PROJECT_DIR) $(wildcard bin) $(wildcard tests) -type f -name \*\.pyc -exec rm {} \;
	@find $(PROJECT_EXTRA_DIRS) $(PROJECT_DIR) $(wildcard bin) $(wildcard tests) -type f -name \*\.py,cover -exec rm {} \;
	@find $(PROJECT_EXTRA_DIRS) $(PROJECT_DIR) $(wildcard bin) $(wildcard tests) -type d -name __pycache__ -empty -delete
	@rm -f *.pyc *.py,cover
	@-rm -fr __pycache__

.PHONY: distclean pydistclean
distclean: pydistclean ##
pydistclean: pyclean
	@rm -fr public/docs public/coverage public/pytest public/reports
	@-rm -fr *.egg-info dist .pytest_cache .coverage
	@-if test -d public ; then rmdir public; fi


# ------------------------------------------------------------------------------
# pypi packages
.PHONY: dist pydist wheel
dist: wheel ##
pydist wheel:
	@-rm -fr *.egg-info dist
	$(PYTHON) -mbuild -xn

# Upload to repository
.PHONY: upload pypi pydist-upload
upload: pypi
pydist-upload: pypi
pypi: pydist
	$(PYTHON) -mtwine check dist/*
	$(PYTHON) -mtwine upload -r $(TWINE_REPOSITORY) $(TWINEFLAGS) dist/*
