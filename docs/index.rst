gitlabsolute
============

Helper and convenience scripts to help with common GitLab actions.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   introduction.rst
   pipeline.rst
   project-badge.rst
   runner.rst

   license.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
